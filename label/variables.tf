variable "zone" {
  type        = string
  description = "dns zone, e.g. epip.io"
}

variable "ns" {
  type        = string
  default     = ""
  description = "organization name, team, or abbrev, e.g. infra, or ops"
}

variable "env" {
  type        = string
  default     = ""
  description = "prod, staging, dev, pre-prod, OR internal, external OR int, ext"  
}

variable "env" {
  type        = string
  default     = ""
  description = "prod, staging, dev OR build, test, deploy, release"
}

variable "name" {
  type        = string
  default     = ""
  description = "app or jenkins"
}

variable "delim" {
  type        = string
  default     = "."
  description = "delimitor"
}

variable "enabled" {
  type        = bool
  default     = true
  description = "set to false to prevent module from creating resources"
}

variable "ctx" {
  type        = object({
    zone      = string
    ns        = string
    env       = string
    env       = string
    name      = string
    enabled   = bool
    delim     = string
    tags      = map(string)
    order     = list(string)
    regex     = string
  })
  default     = {
    zone      = ""
    ns        = ""
    env       = ""
    env       = ""
    name      = ""
    enabled   = true
    delim     = "."
    tags      = {}
    order     = []
    regex     = ""
  }
  description = "context for passing state"
}

variable "tags" {
  type        = map(string)
  description = "additional tags"
}

variable "tags_add" {
  type        = map(string)
  description = "additional tags to append"
}

variable "order" {
  type        = list(string)
  description = "order of labels"
}

variable "regex" {
  type        = string
  default     = "/[^a-zA-Z0-9-_.]/"
  description = "regex to replace chars with empty string"
}
