output "id" {
  value       = local.enabled ? local.id : ""
  description = "Disambiguated ID"
}

output "name" {
  value       = local.enabled ? local.name : ""
  description = "Normalized name"
}

output "ns" {
  value       = local.enabled ? local.ns : ""
  description = "Normalized namespace"
}

output "env" {
  value       = local.enabled ? local.env : ""
  description = "Normalized environment"
}

output "attr" {
  value       = local.enabled ? local.attr : []
  description = "List of attributes"
}

output "delim" {
  value       = local.enabled ? local.delim : ""
  description = "Delimiter between `ns`, `env`, `zone`, `name` and `attr`"
}

output "tags" {
  value       = local.enabled ? local.tags : {}
  description = "Normalized Tag map"
}

output "tags_merged" {
  value       = local.tags_merged
  description = "Additional tags as a list of maps, which can be used in several AWS resources"
}

output "ctx" {
  value       = local.ctx_output
  description = "Context of this module to pass to other label module invocation"
}

output "order" {
  value       = local.order
  description = "The naming order of the id output and Name tag"
}
