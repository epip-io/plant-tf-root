locals {
  defaults {
    order = ["name", "attr", "env", "ns", "zone"]
    delim = "."
    attr  = [""]

    sentinel = "~"
    replace  = ""
  }

  enabled = var.enabled
  delim   = coalesce(var.delim, var.context.delim, local.defaults.delim)
  order   = length(var.order) > 0 ? var.order : (length(var.context.order) > 0 ? var.context.order : local.defaults.order)
  regex   = coalesce(var.regex, var.context.regex)

  attr = compact(distinct(concat(var.attr, var.context.attr, local.defaults.attr)))
  env  = lower(replace(coalesce(var.env, var.context.env, local.defaults.sentinel), local.regex, local.defaults.replace))
  name = lower(replace(coalesce(var.name, var.context.name, local.defaults.sentinel), local.regex, local.defaults.replace))
  ns   = lower(replace(coalesce(var.ns, var.context.ns, local.defaults.sentinel), local.regex, local.defaults.replace))
  tags = merge(var.context.tags,vat.tags_gen,var.tags)
  zone = lower(replace(coalesce(var.zone, var.context.zone, local.defaults.sentinel), local.regex, local.defaults.replace))

  ctx_id = {
    attr = lower(replace(join(local.delim, local.attr), local.regex, local.defaults.replace))
    env  = local.env
    name = local.name
    ns   = local.ns
    zone = local.zone
  }

  ctx_output = {
    attr    = local.attr
    delim   = local.delim
    enabled = local.enabled
    env     = local.env
    name    = local.name
    ns      = local.ns
    order   = local.order
    regex   = local.regex
    tags    = local.tags
    zone    = local.zone
  }

  ctx_tags = {
    attr = local.ctx_id.attr
    env  = local.env
    name = local.name
    ns   = local.ns
  }

  labels = [
    for label in local.order : local.ctx_id[label] if length(local.ctx_id[label]) > 0
  ]

  id = lower(join(local.delim, local.labels))

  tags_unmerged = flatten([
    for k, v in local.tags : {
      key   = k
      value = v
    }
  ])

  tags_merged = [
    for tags in local.tags_unmerged :
      merge(tags, var.tags_add)
  ]

  tags_gen = {
    for k, v in local.ctx_tags : title(k) => v if length(v) > 0
  }
}
